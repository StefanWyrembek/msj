 clear
 close all
 clc

%% Input / Start
decision1 = input('Möchten Sie das generierte Signal [0] oder das eingelesene Signal [1] verwenden?: [0] ','s');
if isempty(decision1)
   decision1 = '0';
end

if (decision1 == '0')
    fs = input('Bitte geben Sie die gewuenschte Abtastfrequenz ein: ');
    fs=round(fs);
    decision2 = input("Möchten Sie Rauschen hinzufügen? Y/N [N]",'s');
    if isempty(decision2)
    decision2 = 'N';
    end
end

cordic = input('Möchten Sie den CORDIC-Algorithmus verwenden? Y/N [Y]: ','s');
if isempty(cordic)
   cordic = 'Y';
end

input1 = input('Plot signal CPFSK ? Y/N [Y]: ','s');


%% CPFSK

if (decision1=='0')
[cpfsk_sig,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs,input1);                      % cpfsk Signal create durch function
clear input1;
end

if (decision1=='1')
    [cpfsk_sig,fA] = audioread('import2.wav');
    fs = 5600;
    N = length(cpfsk_sig);
    n=(0:N-1)';
    maxval = max(abs(fft(cpfsk_sig))/N );
end

if (decision2=="Y")
 input1 = input('How much noise in dB ? [100]: ');
 cpfsk_sig = awgn(cpfsk_sig, input1);
end

%% Bandpass
N_bp = 50;
B=500;
Fpass1 = fs/4-B/2;
Fpass2 = fs/4+B/2;
Fstop1 = Fpass1-150;
Fstop2 = Fpass2+150;

Wstop1 = 3; %(-40dB)
Wpass = 1;  %(0dB)
Wstop2 = 3; %(-40dB)

c = firpm(N_bp,[0 Fstop1 Fpass1 Fpass2 Fstop2 fs/2]/(fs/2), [0 0 1 1 0 0],[Wstop1 Wpass Wstop2]);
fvtool(c,1) 
fileID = fopen('coeff_bp.txt','w');
fprintf(fileID,'%12.8f, ',c);
fclose(fileID);
cpfsk_sig=filter(c,1,cpfsk_sig);


%% Delay-cascade I/Q
% Q-Signal ist das um 90° phasenverschobene I Signal
% 90° = 1/4 einer Schwingung
Tsym=0.04 %(Modulationsfrequenz 25Hz)
N_delays=round((Tsym/4)/(1/fs)); 
I=cpfsk_sig;
Q=N_delay(cpfsk_sig,N_delays);


%% Hilbert-Transformator (REAL/IMAG)
input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
[sig_filt_fir_real,sig_filt_fir_imag, fil_ord] = fir_filter(cpfsk_sig, input1,n, N, maxval, fs);         % apply fir, hilbert
clear input1;

%% komplexe Signalverarbeitung
% sig_fir = sig_filt_fir_real + 1i*sig_filt_fir_imag;                                               % gesamtsignal aus real und imag
% sig_fir_delay=sig_fir.*conj( [0;sig_fir(1:length(sig_fir)-1)]);

sig_fir = I + 1i*Q;                                               % gesamtsignal aus real und imag
sig_fir_delay=sig_fir.*conj( [0;sig_fir(1:length(sig_fir)-1)]);
phasediff=phdiffmeasure(I,Q);
phasedegree = rad2deg(phasediff);

%% Arctan-Demod (atan2/cordic)
if (cordic=='Y')
    theta2=(zeros(length(cpfsk_sig)));
    for n=1:length(cpfsk_sig)
       theta2(n)=arctan_cordic(real(sig_fir_delay(n)),imag(sig_fir_delay(n)),20);
    end
else
    theta2= atan2(imag(sig_fir_delay),real(sig_fir_delay));
end

%% Matched Filter
[rec_sig] = matched_filter(theta2);

rec_sig_unwr = unwrap(rec_sig);

%% Decoder ONLY FOR matched filter
[Decode_signal, Decode_symbol]= decode01(rec_sig,fs,20);

%% Plots
figure(10)
plot(theta2)

figure(20)
plot(rec_sig)

figure(30)
plot(rec_sig_unwr) %unwrap hat keinen Einfluss 

figure(40)
plot(rec_sig,'b')
title('Compare Signals')
hold on
plot(theta2,'r')
hold off

figure(50)
plot (Decode_symbol)

figure(60)
plot(Decode_signal)

figure(70)
plot(I,'b')
title('Compare I/Q')
hold on
plot(Q,'r')
hold off
legend('I','Q')