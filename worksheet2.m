% Worksheet 2
% Allinger, Wyrembek
% 20.04.2020, modified 26.04.2020
% based on cpfsp_generator

clear
close all
clc

%to be set
fA=;
time_length=10000;
symbol_length=20;

%given
amp= 0.5;
fT = fA/4;
fshift = 225 ;
phi_0 = 0;

%dependencies
samples_pro_bit = symbol_length / 1000 * fs;
f_mark=fT+fshift;
f_space=fT-fshift;
repeat=time_length/2*symbol_length;
rect_length = time_length/2*samples_pro_bit;

%rect-signal for given time
rect_sig_math = 2 * ([ones(samples_pro_bit,1);zeros(samples_pro_bit,1)]-1);
for i=1:repeat
    rect_sig = [rect_sig rect_sig_math];
end

%Modulation
n = length(rect_sig);






%Demodulation

signal_to_demodulate=[];

for i=1:length
