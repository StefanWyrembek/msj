/***********************************************************************

 process_one_sample.c

 author    Uli Sauvagerd
 date      20-Apr-20
 revision  1.0

 hardware  D.Module.ADDA8M12 +  D.Module2.C6747

 compiler  CCS 5.50

 history   1.0 initial release 20-Apr-20 by US

 allows to process ADC signal left_in sample-based

           Errors:
           Corrections:
           Comments:

***********************************************************************/

#include <stdio.h>	// for printf
#include <math.h>	// for sin/cos
#include <stdlib.h>	// for FILE*

#include "processor.h"
// Alle Variablen sind i.d.Regel global, daher �ber "extern" bekannt machen
extern FILE* fid_DAC_data;
extern short left_in, xn, I_signal, Q_signal, I_sigT[], Q_sigT[], IQ_add, sig_demod, sig_opt;
extern short delay_mem[], fir_mem[], match_mem[];
extern short int N;

// prototype
void process_one_sample();
short hilbert_fir(short memory[],short int N_T, short xn ); //Hilbert Transformator => FIR Filter
short N_delay(short delay[], short int N_T, short xn);    //Hilbert Transformator => Verz�gerer
short fm_realadd(short I_signal, short I_sigT[], short Q_signal, short Q_sigT[]);    //FM Demod mit realer Signalverarbeitung => Teil 1 / links
short fm_realdemod(short IQ_add);                   //FM Demod mit realer Signalverarbeitung => Teil 2 / recht
short fm_arctandemod(short I_signal, short Q_signal); //FM Demod mit atan2
short matchedf(short match_mem[], short int N_T, short sig_demod);

int debugcounter = 1;
// function body
void process_one_sample()
{ 
// linker Kanal wird mit 0.5 multipliziert 
    left_in /= 2;
    xn = left_in;
// Hilbert Filter in ANSI C
    I_signal = N_delay(delay_mem, N, xn); //reiner Verzoegerer, Real/Inline
    Q_signal = hilbert_fir(fir_mem, N, xn); //FIR Filter des Hilb.-Transf., Imag/Quadrature

// FM Demodulator
// (...) Ergebnis steht in y_CPFSK_demod 
    
    //IQ_add = fm_realadd(I_signal, I_sigT, Q_signal, Q_sigT);
    //sig_demod = fm_realdemod(IQ_add);

    sig_demod = fm_arctandemod(I_signal, Q_signal);

// Matched Filter

    sig_opt = matchedf(match_mem, N, sig_demod);


#ifdef USE_MSVC_ANSI_C_SIM
// Schreiben in DAC Datei, aber nur im Falle von ANSI C
//    fprintf(fid_DAC_data, "%hd\n", sig_demod); // y_CPFSK_demody
  fprintf(fid_DAC_data, "%hd\n", sig_opt); // y_CPFSK_demody
#endif
  debugcounter++;
}
