/***********************************************************************

 processor.h

 Steuerdatei f�r Wetterdaten_Antenne Projekt

 author    Uli Sauvagerd
 date      15-Dez-19
 revision  1.0

 hardware  D.Module.ADDA8M12 +  D.Module2.C6747

 compiler  CCS 5.50

 history   1.0 initial release 17-Jun-19 by US

***********************************************************************/

//#define USE_HARDWARE_ADDA8M12_C6747
#define USE_MSVC_ANSI_C_SIM

#ifdef USE_MSVC_ANSI_C_SIM
#define _CRT_SECURE_NO_WARNINGS
#endif
