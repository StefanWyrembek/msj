#include "hil_coeff.h"
#include "math.h"

float roundfloat(float number);

short hilbert_fir(short memory[], short int N_T, short xn) {
	short i;
	float yyy=0;
	float yy = 0;
	short output;

	memory[N_T] = xn;
	for (i = 0; i < N_T; i++) {
		yyy += memory[N_T - i] * hil_coeff[i]; //2* 16 bit Multi == 32 bit mit zwei Vorzeichenbit
	}

	for (i = 1; i <= N_T+1; i++) {
		memory[i - 1] = memory[i];
	}

	// 
	yy = roundfloat(yyy);
	output = (short)(yy);
	return output;
}
