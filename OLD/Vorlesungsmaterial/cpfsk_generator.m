% cpfsk_generator.m
%
% US 10-Apr-2019
% 
% MSJ SS 2019 : Bandbreite des CPFSK Signals bestimmen
clear
close all
clc

% Parameter
amplitude = 1;
fs = 5600;
fT = fs/4;
half_of_freq_shift = 225 ;
phi_0 = 0;

% Ein Bit hat eine L�nge von 20 msec, also 0.02 sec. Die 30 msec vom
% Stopbit ignorieren wir erstmal...
disp('--------------------------------------------')
samples_pro_bit = 0.02 * fs;
fprintf('samples_pro_bit : %d\n', samples_pro_bit);

rect_1_to_modulate_basic = 2 * ([ones(samples_pro_bit,1);zeros(samples_pro_bit,1)]-0.5);
num_rect_1_to_modulate_basic = length(rect_1_to_modulate_basic);

fprintf('L�nge einer 0/1 Bit Folge : %d\n', length(rect_1_to_modulate_basic) );

fm = 1/length(rect_1_to_modulate_basic)*fs;
fprintf('Modulationsfrequenz, wenn es immer mit 0/1/0/1 usw weitergeht : %d Hz\n', fm );
disp('--------------------------------------------')
rect_1_to_modulate = [];

% 10 Pakete hintereinander ...
num_intervals = 10;
for kx=1:num_intervals
    rect_1_to_modulate = [rect_1_to_modulate; rect_1_to_modulate_basic];
end;
N = length(rect_1_to_modulate);
n=(0:N-1)';

num=[0,1];
den=[1,-1];
triag_to_modulate = filter(num, den, rect_1_to_modulate);

cpfsk_sig         = amplitude .*  sin(2 * pi * fT * n/fs + 2 * pi * half_of_freq_shift * triag_to_modulate/fs + phi_0);
cpfsk_sig_carrier = amplitude .*  sin(2 * pi * fT * n/fs + phi_0);
maxval = max(abs(fft(cpfsk_sig))/N );

figure(1);
set(gcf,'Units','normal','Position',[0.1 .4 .4 .4])
plot(n, cpfsk_sig_carrier,'b.-', n, rect_1_to_modulate,'r-'),grid
axis([0,N,-1.2,1.2]);

figure(2);
set(gcf,'Units','normal','Position',[0.2 .4 .4 .4])
plot(n/N*fs, abs(fft(cpfsk_sig))/N/maxval),grid

figure(3);
set(gcf,'Units','normal','Position',[0.3 .4 .4 .4])
freq = n/N*fs;
subplot(2,1,1);
stem(freq, abs(fft(cpfsk_sig))/N/maxval,'filled'),grid
axis([900,1800,-0.1, +1.2]);
subplot(2,1,2);
stem(freq, abs(fft(cpfsk_sig))/N/maxval,'filled'),grid
axis([900,1800,-0.1, +0.2]);


