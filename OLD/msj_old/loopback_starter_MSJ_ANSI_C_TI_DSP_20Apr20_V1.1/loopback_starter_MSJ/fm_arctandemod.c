short fm_arctandemod(short I_signal, short Q_signal) {
	short y;
	y = atan2(Q_signal / I_signal);
	return y;
}