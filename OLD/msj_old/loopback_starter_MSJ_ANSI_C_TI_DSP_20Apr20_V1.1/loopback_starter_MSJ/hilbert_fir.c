#include "hil_coeff.h"
short hilbert_fir(short memory[], short int N_T, short xn) {
	short i;
	float yyy=0;
	short output;

	memory[N_T] = xn;
	for (i = 1; i < N_T; i++) {
		yyy += memory[N_T - i] * hil_coeff[i]; //2* 16 bit Multi == 32 bit mit zwei Vorzeichenbit
	}

	for (i = 1; i < N_T; i++) {
		memory[i - 1] = memory[i];
	}

	// 32 bit zu short int (16 bit) == shift left 15 bit
	output = (short) round(yyy);
	return output;
}