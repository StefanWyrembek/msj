short matchedf(short match_mem[], short int N_T, short sig_demod) {
	int i,j;
	short b[200];
	for (j = 1; j <= 200; j++) {
		b[j - 1] = 1;
	}

	int yyy = 0;
	short output;

	match_mem[N_T - 1] = sig_demod;
	for (i = 1; i < N_T; i++) {
		yyy += match_mem[N_T - 1 - i] * b[i]; //2* 16 bit Multi == 32 bit mit zwei Vorzeichenbit
	}

	for (i = 1; i < N_T; i++) {
		match_mem[i - 1] = match_mem[i];
	}

	// 32 bit zu short int (16 bit) == shift left 15 bit
	output = (short)(yyy >> 15);
	return output;
}