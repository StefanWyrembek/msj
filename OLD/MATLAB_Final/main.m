clear
close all
clc


fs = input('Bitte geben Sie die gewuenschte Abtastfrequenz ein: ');
input1 = input('Plot signal CPFSK ? Y/N [Y]: ','s');

[cpfsk_sig,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs,input1);                      % cpfsk Signal create durch function
clear input1;

input1 = input('How much noise in dB ? [100]: ');
cpfsk_sig = awgn(cpfsk_sig, input1);


input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
[sig_filt_fir_real,sig_filt_fir_imag, fil_ord] = fir_filter(cpfsk_sig, input1,n, N, maxval, fs);         % apply fir, hilbert
clear input1;

sig_fir = sig_filt_fir_real + j*sig_filt_fir_imag;                                               % gesamtsignal aus real und imag
sig_fir_delay=sig_fir.*conj( [0;sig_fir(1:length(sig_fir)-1)]);
theta2= atan2(imag(sig_fir_delay),real(sig_fir_delay));

[rec_sig] = matched_filter(theta2);

rec_sig_unwr = unwrap(rec_sig);

figure(10)
plot(theta2)

figure(20)
plot(rec_sig)

figure(30)
plot(rec_sig_unwr) %unwrap hat keinen Einfluss 


