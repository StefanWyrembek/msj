clear
close all
clc


fs = input('Bitte geben Sie die gewuenschte Abtastfrequenz ein: ');
input1 = input('Plot signal CPFSK ? Y/N [Y]: ','s');

[cpfsk_sig,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs,input1);                      % cpfsk Signal create durch function
clear input1;

input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
[sig_filt_fir_real,sig_filt_fir_imag, fil_ord] = fir_filter(cpfsk_sig, input1,n, N, maxval, fs);         % apply fir, hilbert
clear input1;

sig_fir = sig_filt_fir_real + j*sig_filt_fir_imag;                                               % gesamtsignal aus real und imag

%theta = atan(sig_fir);                                              % theta (phasenwinkel) berechnen
%theta_deg = rad2deg(theta);
theta2 = atan2(sig_filt_fir_imag,sig_filt_fir_real); 
theta2_deg2 = rad2deg(theta2);

z1 = fmdemod(cpfsk_sig, fT, fs, 225);
z2 = (1/fs)*[zeros(1,size(sig_fir,2)); diff(unwrap(theta2))];
z3 = ((1/(2*pi*225))*[zeros(1,size(sig_fir,2)); diff(unwrap(angle(sig_fir)))*fs]);
figure(10)
plot(z1)
figure(11)
plot(z3)






