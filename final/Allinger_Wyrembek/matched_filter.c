/***********************************************************************
  
  @Stefan Wyrembek
  @Michael Allinger
  
  matched_filter.c

  Autor						Stefan Wyrembek, Michael Allinger

  Datum						07.06.2020

  compiler					xxxxxx

  Matched Filterfunktion

***********************************************************************/

#include "../loopback_starter_MSJ/matched_coeff.h"

float matched_filter(short FIR_delays_matched[], short int N_delays_matched, short x_n) {
	short i;
	float y;
	float FIR_accu32 = 0;
	//---------------------------------------------------------------------
	// delays BACKWARDS, coefficients in FORWARD direction
	FIR_delays_matched[N_delays_matched - 1] = x_n;	// read input sample from ADC 
// accumulate in 32 bit variable
	FIR_accu32 = 0;				// clear accu
	for (i = 0; i < N_delays_matched; i++)		// FIR filter routine
		FIR_accu32 += FIR_delays_matched[N_delays_matched - 1 - i] * matched_coeff[i];

	// loop to shift the delays
	for (i = 1; i < N_delays_matched; i++)
		FIR_delays_matched[i - 1] = FIR_delays_matched[i];

	// shift back by 15 bit to obtain short int 16 bit output 
	
	y = (float)(FIR_accu32);
	return y;
}