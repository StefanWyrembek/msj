/***********************************************************************

 @Stefan Wyrembek
 @Michael Allinger

 process_one_sample.c

 author                 Uli Sauvagerd

 Erweiterung            Stefan Wyrembek, Michael Allinger
 
 date                   07-Jun-20

 revision               1.0

 hardware               D.Module.ADDA8M12 +  D.Module2.C6747

 compiler               xxxxxx

 history                1.0 initial release 20-Apr-20 by US

 allows to process ADC signal left_in sample-based

           Errors:
           Corrections:
           Comments:

***********************************************************************/

#include <stdio.h>	// for printf
#include <math.h>	// for sin/cos
#include <stdlib.h>	// for FILE*

#include "processor.h"
// Alle Variablen sind i.d.Regel global, daher �ber "extern" bekannt machen
extern FILE* fid_DAC_data;
extern short left_in;
short x_n;

//define fir filter for quadratur
short FIR_delays[23];
short int N_delays = 23;
short FIR_delays_matched[112];
short int N_delays_matched = 112;


//define delay for in phase
short delay_i[11]; 
short int N_T_i = 11;

short delay_ii[1];
short int N_T_ii = 1;



short x_in_phase;
short x_quad;
short x_in_phase_o; // oberer Zweig
short x_quad_o;     // oberer Zweig
short x_in_phase_u; // unterer Zweig
short x_quad_u;     // unterer Zweig
short Y_I;
short Y_Q;
short out_dac;

float sig;

// prototypes
void process_one_sample();
short FIR_filter(short FIR_delays[],short int N_delays, short x_n);
short delay_in_phase(short delay_i[], short int N_T_i, short x_n);
float matched_filter(short FIR_delays_matched[], short int N_delays_matched, short x_n);



// function body
void process_one_sample()
{
// linker Kanal wird mit 0.5 multipliziert 

    left_in /= 2;

    // linker Kanal zu x_n --> reelles Signal
   x_n = left_in;

   // 
   x_quad = FIR_filter(FIR_delays, N_delays, x_n);
   x_in_phase = delay_in_phase(delay_i, N_T_i, x_n);
  
   /*
   
       --------x_in_phase ---- Hilbert
       -                                ----------YQ
       -
       -    ---x_quad --------Delay N/2
       -    -
       -    -
       -    ---Hilbert--------
       -                        -      -----------YI
       -----Delay N/2 --------  +
     
   */

   x_in_phase_o = FIR_filter(FIR_delays, N_delays, x_in_phase);
   x_quad_o = delay_in_phase(delay_i, N_T_i, x_quad);


   x_quad_u = FIR_filter(FIR_delays, N_delays, x_quad);
   x_in_phase_u = delay_in_phase(delay_i, N_T_i, x_in_phase);

   
   Y_I = x_quad_u + x_in_phase_u;
   Y_Q = x_in_phase_o - x_quad_o;
   
   x_n = atan2(Y_Q, Y_I);

   
   sig = matched_filter(FIR_delays_matched, N_delays_matched, x_n);
   out_dac = sig * 32768;

// Hilbert Filter in ANSI C
// (...)
// FM Demodulator
// (...) Ergebnis steht in y_CPFSK_demod 
	
#ifdef USE_MSVC_ANSI_C_SIM
// Schreiben in DAC Datei, aber nur im Falle von ANSI C
    fprintf(fid_DAC_data, "%hd\n", out_dac); // y_CPFSK_demody
#endif
}