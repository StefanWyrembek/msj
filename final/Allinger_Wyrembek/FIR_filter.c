/***********************************************************************
  
  @Stefan Wyrembek
  @Michael Allinger
  
  FIR_filter.c

  Autor						Stefan Wyrembek, Michael Allinger

  Datum						07.06.2020

  compiler					xxxxxxx

  FIR Filterfunktion

***********************************************************************/

#include "../loopback_starter_MSJ/hil_coeff.h"



short FIR_filter(short FIR_delays[],short int N_delays, short x_n) {
	short i, y;
	float FIR_accu32 = 0;
	//---------------------------------------------------------------------
	// delays BACKWARDS, coefficients in FORWARD direction
	FIR_delays[N_delays - 1] = x_n;	// read input sample from ADC 
// accumulate in 32 bit variable
	FIR_accu32 = 0;				// clear accu
	for (i = 0; i < N_delays; i++)		// FIR filter routine
		FIR_accu32 += FIR_delays[N_delays - 1 - i] * hil_coeff[i];

	// loop to shift the delays
	for (i = 1; i < N_delays; i++)
		FIR_delays[i - 1] = FIR_delays[i];

	// shift back by 15 bit to obtain short int 16 bit output 
	
	y = (short)(FIR_accu32);
	
	return y;
}