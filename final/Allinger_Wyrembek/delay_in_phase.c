/***********************************************************************
  
  @Stefan Wyrembek
  @Michael Allinger
  
  delay_in_phase.c

  Autor						Stefan Wyrembek, Michael Allinger

  Datum						07.06.2020

  compiler					xxxxxx

  Delay Funktion

***********************************************************************/

short delay_in_phase(short delay[], short int N_T, short xn) {
	int j;
	short out;

	out = delay[0];
	for (j = 1; j - 1 < N_T; j++) {
		delay[j - 1] = delay[j];
	}
	delay[N_T - 1] = xn;

	return out;
}