/***********************************************************************
  
  @Michael Allinger
  @Stefan Wyrembek
  
  bp_filter.c

  Autor						Michael Allinger, Stefan Wyrembek

  Datum						09.06.2020

  compiler					xxxxxxx

  FIR BP-Filterfunktion

***********************************************************************/

#include "coeff_bp.h"

short bp_filter(short bp_delays[], short int N_bp_delays, short x_n) {
	short i, y;
	float FIR_accu32 = 0;
	//---------------------------------------------------------------------
	// delays BACKWARDS, coefficients in FORWARD direction
	bp_delays[N_bp_delays - 1] = x_n;	// read input sample from ADC 
// accumulate in 32 bit variable
	FIR_accu32 = 0;				// clear accu
	for (i = 0; i < N_bp_delays; i++)		// FIR filter routine
		FIR_accu32 += bp_delays[N_bp_delays - 1 - i] * bp_coeff[i];

	// loop to shift the delays
	for (i = 1; i < N_bp_delays; i++)
		bp_delays[i - 1] = bp_delays[i];

	// shift back by 15 bit to obtain short int 16 bit output 

	y = (short)(FIR_accu32);

	return y;
}