function [theta,theta_test] = atan2owncordic2(Y,X,iter)
%% ATAN2OWNCORDIC 
% Basierend auf der Dokumentation von STMircoelectronix
% => Arctangent => Vector =>  x=(1/An)x0, y=(1/An)y0, z=0 !=> Reduce y to Zero
% Basierend auf John Burkardt(2007) und Jean-Michel Muller: Elementary Functions: Algorithms and Implementation
% 
% Beschreibung:
% Ziel ist die Phasenbestimmung durch Annäherung von Y an 0.
% Dazu wird bei jeder Iteration geguckt, ob y positiv oder negativ ist und
% entsprechend das Vorzeichen (als Variable z) der Rotationsrichtung (also
% +-°) gesetzt.
% je nach Literatur gibt es zwei Möglichkeiten, die endgültige Phase im 4
% Quadrantenbereich zu berechnen:
% Addition der vorweg abgezogenen Phase
% einmalige Multiplikation des Phasenergebnisses mit einem Vorzeichen (hier
% auch Vorzeichen genannt)
% auf eine Kompensation der Amplitude wird verzichtet, da nur die Phase
% relevant ist
%
% Ausgabe:
% theta in rad => Literatur
% theta_test in rad => eigene Kompensation des Startquadranten

% Abfangen, dass mehr Iterationen gemacht werden, als die LUT (siehe unten) hergibt!
if iter>100
    iter=100;
end


% erstmal die Phase auf 0
phase=0;
theta=0;
theta_test=0;
%variable (hochZwei) zur Darstellung 1/(2^i)  => In C Bitshift!
hZ=1; %Start 1/(2^0)=1

%laden der LUT (Look-Up-Table) enthält eine Tabelle in Form eines Arrays
%für atan( 1 / (2^i) ) mit i = 0 : MAX_ITERATIONEN
%MAX_ITERATIONEN ist aktuell 100. Je nach Literatur zwischen 15 und 20
%Durchläufen mindestens nötig für Genauigkeit.
load("owncordig_LUT.mat","owncordig_LUT");

%Welcher Quadrant?

% Wenn im 3. Quadranten (lu)
if (X<0) && (Y<0)
    x=-X;
    y=-Y;
    inital_rotate=pi;
    vorzeichen=1;
%wenn im 2. Quadranten
elseif (X<0) && (Y>0)
    x=-X;
    y=Y;
    vorzeichen=-1;
    inital_rotate=pi/2;
%wenn im 4. Quadranten
elseif (X>0) && (Y<0)
    y=-Y;
    x=X;
    vorzeichen=-1;
    inital_rotate=3*pi/2;
%wenn im ersten Quadranten
else
    x=X;
    y=Y;
    vorzeichen=1;
    inital_rotate=0;
end

% eigentlicher Algorithmus
for k=1:iter
    %wenn der Startvektor negativ ist (erst möglich ab 2. Iteration, wenn
    %erste Drehung des Zeigers/Vektors "zu weit" ging... dann wollen wir
    %wieder zurück in den ersten Quadranten! <= um y=0 abzufangen!
    if (y<=0)
        z=1;
    else
        z=-1;
    end
    
    delta_phase=owncordig_LUT(k);
    
    % Nun folgen die Berechnungen für x+1 und y+1 (mit k =
    % Iterationsschritt (nach Gerdson-Kröger/STMirco...)
    % x+1 = x -  z * y / 2^k oder auch x+1 = x - [Rotationsrichtung] (y>>k)
    % y+1 = z * x / 2^k + y oder auch y+1 = [Rotationsrichtung] (x>>k) +y
    
    x1=x-z*hZ*y;
    y1=z*hZ*x+y;
    phase=phase-z*delta_phase;
    
    %Vorbereitung neue Iteration
    x=x1;
    y=y1;
    
    %Erhöhung des 1/2^k mit k+1 => halbiert / bitshift
    hZ=hZ/2;
end

%Berechnung der eigentlichen Phase
theta_test=phase+inital_rotate; %Idee im Kopf
theta=vorzeichen*phase; %Vorgehensweise der Literatur
end
