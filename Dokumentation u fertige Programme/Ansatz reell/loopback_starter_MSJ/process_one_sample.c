/***********************************************************************

 @Stefan Wyrembek
 @Michael Allinger

 process_one_sample.c

 author                 Uli Sauvagerd

 Erweiterung            Stefan Wyrembek, Michael Allinger
 
 date                   07-Jun-20

 revision               1.0

 hardware               D.Module.ADDA8M12 +  D.Module2.C6747

 compiler               xxxxxx

 history                1.0 initial release 20-Apr-20 by US

 allows to process ADC signal left_in sample-based

           Errors:
           Corrections:
           Comments:

***********************************************************************/

#include <stdio.h>	// for printf
#include <math.h>	// for sin/cos
#include <stdlib.h>	// for FILE*

#include "processor.h"
// Alle Variablen sind i.d.Regel global, daher �ber "extern" bekannt machen
extern FILE* fid_DAC_data;
extern short left_in;
short x_n;

//define fir filter for quadratur
short FIR_delays[23];
short int N_delays = 23;
short FIR_delays_matched[200];
short int N_delays_matched = 200;


//define delay for in phase
short delay_i[11]; 
short int N_T_i = 11;

short delay_ii[1];
short int N_T_ii = 1;



short x_i;
short x_q;
short x_i_d;
short x_q_d;
short x_i_d_c;
short x_q_d_c;

double y_i;
double y_q; 

float y;

float y_matched;


// prototypes
void process_one_sample();
short FIR_filter(short FIR_delays[],short int N_delays, short x_n);
short delay_in_phase(short delay_i[], short int N_T_i, short x_n);
float matched_filter(short FIR_delays_matched[], short int N_delays_matched, short x_n);



// function body
void process_one_sample()
{
    // linker Kanal wird mit 0.5 multipliziert 

    left_in /= 2;

    // linker Kanal zu x_n --> reelles Signal
    x_n = left_in;

    // 
    x_q = FIR_filter(FIR_delays, N_delays, x_n);
    x_i = delay_in_phase(delay_i, N_T_i, x_n);

    /*

        --------x_in_phase ---- Hilbert
        -                                ----------YQ
        -
        -    ---x_quad --------Delay N/2
        -    -
        -    -
        -    ---Hilbert--------
        -                        -      -----------YI
        -----Delay N/2 --------  +

    */

    x_i_d = delay_in_phase(delay_ii, N_T_ii, x_i);
    x_q_d = delay_in_phase(delay_ii, N_T_ii, x_q);

    x_i_d_c = x_i_d;
    x_q_d_c = -x_q_d;

    y_i = x_i - x_q_d_c;
    y_q = x_q + x_i_d_c;


    y_q = y_q / (2 ^ 15);
    y_i = y_i / (2 ^ 15),

    y = atan2(y_q, y_i);
    
   
 y_matched = matched_filter(FIR_delays_matched, N_delays_matched, y);
   
   //out_dac = sig * 32768;

// Hilbert Filter in ANSI C
// (...)
// FM Demodulator
// (...) Ergebnis steht in y_CPFSK_demod 
	
#ifdef USE_MSVC_ANSI_C_SIM
// Schreiben in DAC Datei, aber nur im Falle von ANSI C
    fprintf(fid_DAC_data, "%hf\n", y); // y_CPFSK_demody
#endif
}