/***********************************************************************
  
  @Stefan Wyrembek
  @Michael Allinger
  
  delay_in_phase.c

  Autor						Stefan Wyrembek, Michael Allinger

  Datum						07.06.2020

  compiler					xxxxxx

  Delay Funktion

  Beschreibung:
  Diese Delay-Funktion (= Verzögerer) 

***********************************************************************/

short delay_in_phase(short delay[], short int N_T, short xn) {
	int j;
	short out;

	//Zuerst wird das um N_T verzögerte Sample ausgegeben
	out = delay[0];

	//Dann werden alle Samples um 1 verzögert und das bereits um N_T verzögerte überschrieben. 
	for (j = 1; j - 1 < N_T; j++) {
		delay[j - 1] = delay[j];
	}
	//Am Ende wird das "neue" Sample im Verzögerer gespeichert
	delay[N_T - 1] = xn;	//N_T-1, da in C Arrays bei 0 starten und nicht bei 1. => 0 bis N_T-1 = N_T

	return out;
}