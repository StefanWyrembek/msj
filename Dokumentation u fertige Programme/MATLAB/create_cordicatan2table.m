close all
clear
clc

%% Pre-Computing / Vorberechnung von atan(1/(2^i)) mit i=0:1:MAX_ITERATIONEN

MAX_ITERATIONEN=100; % ab 20 wird es genau genug

% da i und j mit komplexen Zahlen/Imaginärteil verbunden werden,
% loopcounter ist k

owncordig_LUT=zeros(MAX_ITERATIONEN,1);
iter=MAX_ITERATIONEN-1;
for k=0:1:iter
    owncordig_LUT(k+1)=atan(1/(2^k));
end

save("owncordig_LUT.mat","owncordig_LUT");