function newsignal = N_delay(signal,N_delays)
%*****************************************************************************80
%
%% N_DELAY
%
%  Modified:
%
%    14 June 2007
%
%  Author:
%
%    John Burkardt
%
%  Parameters:
%
%    Input, signal, is the signal-array to be delayed
%
%    Input, N_delays, is the number of delays to be added.
%    
%    Output, newsignal, is the with N delays delayed signal.
%
%  Local Parameters:
%
%    i (Counter)

newsignal=(zeros(length(signal),1));
for i=(N_delays+1):1:length(signal)
    newsignal(i)=signal(i-N_delays);
end

end
