%%Skript zur Berechnung aller möglichen Abtastfrequenzen
%%Wyrembek & Allinger

clear all
clc

fT = 10100800;                      %Traegerfrequenz 
fA = 10000;                         %Abtastfrequenz; Fuer Berechnung nicht relevant
m = 1:1:10000;                      %Teiler
k = 1+4.*m;                         %Eigentlich k*
fA_possible_4 = ((fT./k)*4)';       %lListe aller moeglichen Frequenzen
fa_N = (fT./fA_possible_4-0.25);    %Berechnung von N; N sollte ein Integer sein !


