function [decoded_signal,decoded_symbol] = decode_diff(signal,fs,Tsym)
%DECODE_DIFF uses a differentiator to decode 0/1
%   Missing synchronisation!

decoded_signal=diff(signal); % Länge = signal - 1
decoded_signal(end+1,:)=0;
samples_per_symbol=Tsym * fs;
decoded_symbol=zeros(round((length(signal)/samples_per_symbol)),1);

for k=1:length(decoded_symbol)
    start=((k-1)*round(samples_per_symbol))+1;
    ende=k*samples_per_symbol;
    sum=0;
    for l=start:1:ende
        sum=sum+decoded_signal(l);
    end
    decision=sum/samples_per_symbol;
    if decision>0
        decoded_symbol(k)=1;
    else
        decoded_symbol(k)=0;
    end


end

