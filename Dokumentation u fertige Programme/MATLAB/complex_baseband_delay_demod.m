function [y, t_c1, t_c2] = complex_baseband_delay_demod(x_i,x_q, cordic)
%Basisband Delay Modulation eines komplexen Signals mit atan2()
%Input --> (real(x),imag(x))
    
    x = x_i + 1i*x_q;

    N = length(x);
    
    x_d_c = conj([0;x(1:N-1)]);            % cpfsk -> conj(z^-1)

    mul = x_d_c.*x;                        % cpfsk * conj(z^-1)

    y = atan2(imag(mul),real(mul));        % phase berechnen
    
    if (cordic==1)
        t_c1=cordicatan2(imag(mul),real(mul),21);
        t_c2=zeros(length(x_i),1);
        im=imag(mul);
        re=real(mul);
        for k=1:length(im)
            [t_c2(k),~]=atan2owncordic(im(k),re(k),21);
        end
    else
        t_c1=0;
        t_c2=0;
    end
    
end