function [rec_sig] = matched_filter(theta2,fs)
%MATCHED_FILTER Summary of this function goes here
%   Detailed explanation goes here

s = fs*0.02;
s = round(s);    
b = [ones(s,1)]/s;
rec_sig = filter(b,1,theta2); 
end

