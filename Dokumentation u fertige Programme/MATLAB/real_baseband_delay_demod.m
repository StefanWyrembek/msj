function [y, t_c1, t_c2] = real_baseband_delay_demod(x_i, x_q, cordic)
%Basisband Delay Modulation eines reellen Signals mit atan2()
%Input --> (real(x),imag(x))
    
    N = length(x_i);

    x_i_d = [0;x_i(1:N-1)];     % z^-1 delay
    x_q_d = [0;x_q(1:N-1)];     % z^-1 delay
    
    x_i_d_c = x_i_d;            % conj --> realteil bleibt gleich
    x_q_d_c = -x_q_d;           % conj

    yi = x_i - x_q_d_c;         %% cross add
    yq = x_q + x_i_d_c;         %% cross add
    
    y = atan2(yq,yi);           % calc phase
    
    if (cordic==1)
        t_c1=cordicatan2(yq,yi,21);
        t_c2=zeros(length(x_i),1);
        for k=1:length(x_i)
            [t_c2(k),~]=atan2owncordic(yq(k),yi(k),21);
        end
    else
        t_c1=0;
        t_c2=0;
    end

end

