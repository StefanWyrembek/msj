function [binarysignal,binarysymbol] = decode_easy01(signal,fs,Tsym)
%DECODE01 Summary of this function goes here
%   Detailed explanation goes here
    fs=round(fs);
    binarysignal=zeros(1,length(signal));
    estimatedSymbols=round(length(signal)*((Tsym/1000)/(1/fs)));
    binarysymbol=zeros(1,estimatedSymbols);
    memory=[0 0];
    l=1;
    while (l<(length(signal)+1))
       memory(2)=signal(l);
       if (memory(1)>memory(2))
           binarysignal(l)=0;
       elseif (memory(1)<memory(2))
           binarysignal(l)=1;
       else
           if (l>1)
               binarysignal(l)=binarysignal(l-1);
           else
               binarysignal(l)=0;
           end
       end
       memory(1)=memory(2);
       l=l+1;
    end
    
    startuptime=round((Tsym*fs)/2);
    j=startuptime;
    k=1;
    
    for j=startuptime:1:length(binarysignal)
        if((binarysignal(j))==0)
            if((binarysignal(j-1))==1&&(binarysignal(j-2))==1&&(binarysignal(j-3))==1)
                binarysymbol(k)=1;
                k=k+1;
            end
        else
            if((binarysignal(j-1))==0&&(binarysignal(j-2))==0&&(binarysignal(j-3))==0)
                binarysymbol(k)=0;
                k=k+1;
            end
        end
     end

end

