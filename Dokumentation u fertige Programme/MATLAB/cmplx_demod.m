function [y, t_c1, t_c2] = cmplx_demod(x, fs, cordic)

%freq = (-999:999)/2000;

b_Koeff= firpm(200,[0 0.423 0.473 0.527 0.577 1],[0 0 1 1 0 0]); % Berechnung der Filter-Koeffizinten
%fvtool(b_Koeff,1) 
%H_fir_BP =freqz(b_Koeff, 1, 2*pi*freq);
%figure(10);
%plot(freq*fA, abs(H_fir_BP)),grid
%title('Amplitudengang des Filters');
%xlabel('f (Hz)')
%ylabel('Die Amplitude')
% das verschobenen Filter
k=0:200;
b_Koeff_shifted= b_Koeff.*exp(2*1i*pi*k.*(225)/fs);
%fvtool(b_Koeff_shifted,1) 
%H_shifted =freqz(b_Koeff_shifted, 1, 2*pi*freq);
%figure(11);
%plot(freq*fA, abs(H_shifted)),grid
%title('Amplitudengang des verschobenen Filters');
%xlabel('f (Hz)')
%ylabel('Die Amplitude')
%[x,fs] = audioread('import2.wav');
%recieved_samples = cpfsk_sig;
cpfsk_kom=filter(b_Koeff_shifted,1,x);
cpfsk_kom_delayed = conj([0;cpfsk_kom(1:length(cpfsk_kom)-1)]);
y_FM_demod=cpfsk_kom.*cpfsk_kom_delayed;
y = atan2(imag(y_FM_demod),real(y_FM_demod));
%figure(12);
%plot(y_cpfsk_out);

%figure
%plot(ma)
%title('Das demodulierte CPFSK-Signal mit einem komplexen FIR-Filter (N=178)')
%xlabel('Anzahl der Samples')
%ylabel('Die Amplitude')
if (cordic==1)
    t_c1=cordicatan2(imag(y_FM_demod),real(y_FM_demod),21);
    im=imag(y_FM_demod);
    re=real(y_FM_demod);
    t_c2=zeros(length(im),1);
    for k=1:length(im)
        [t_c2(k),~]=atan2owncordic(im(k),re(k),21);
    end
else
    t_c1=0;
    t_c2=0;
end
end