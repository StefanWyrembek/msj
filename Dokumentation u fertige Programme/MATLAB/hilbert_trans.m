function [sig_filt_fir_real, sig_filt_fir_imag] = hilbert_trans(cpfsk_sig)
    fil_ord = 22;
    b=firpm(fil_ord,[0.2 0.8],[1 1], 'hilbert');                                 %berechnuhng der Koeffizienten des Filters
    fvtool(b,1)                                                             %Amplitudengang des Filters
    sig_filt_fir_imag = filter(b,1,cpfsk_sig);                              %das ist der Imaginärpart des Analytischen Signals
    sig_filt_fir_real = [zeros(11,1); cpfsk_sig(1:length(cpfsk_sig)-fil_ord/2)];   %das ist der Realpart des Analytischen Signals
    %sig_fir = sig_filt_fir_real+1i*(sig_filt_fir_imag);
    
%     if input1 == "Y"
% 
%     figure(4);
%     set(gcf,'Units','normal','Position',[0.2 .4 .4 .4])
%     plot(n/N*fs, abs(fft(sig_fir))/N/maxval),grid
% 
%     end
    
end