function [y, t_c1, t_c2] = real_cmplx_demod(x, fs, cordic) 
    
    N = length(x);
    
    b_Koeff= firpm(200,[0 0.423 0.473 0.527 0.577 1],[0 0 1 1 0 0]);
    
    k=0:200;
    
    b_Koeff_shifted= b_Koeff.*exp(2*1i*pi*k.*(225)/fs);
    
    k_i = real(b_Koeff_shifted);
    k_q = -imag(b_Koeff_shifted);
    
    x_i = filter(k_i,1,x); 
    
    x_q = filter(k_q,1,x); 
    
    x_i_d = [0;x_i(1:N-1)];     % z^-1 delay
    x_q_d = [0;x_q(1:N-1)];     % z^-1 delay
    
    x_i_d_c = x_i_d;            % conj --> realteil bleibt gleich
    x_q_d_c = -x_q_d;           % conj

    yi = x_i - x_q_d_c;         %% cross add
    yq = x_q + x_i_d_c;         %% cross add
    
    y = atan2(yq,yi);           % calc phase
    
    if (cordic==1)
        t_c1=cordicatan2(yq,yi,21);
        t_c2=zeros(length(x_i),1);
        for k=1:length(x_i)
            [t_c2(k),~]=atan2owncordic(yq(k),yi(k),21);
        end
    else
        t_c1=0;
        t_c2=0;
    end
    
end
    
    
    
