fs = 9978.562608051370;

[cpfsk_sig,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs);    

freq = (-999:999)/2000;
fA = fs
b_Koeff= firpm(178,[0 0.423 0.473 0.527 0.577 1],[0 0 1 1 0 0]); % Berechnung der Filter-Koeffizinten
fvtool(b_Koeff,1) 
H_fir_BP =freqz(b_Koeff, 1, 2*pi*freq);
figure(10);
plot(freq*fA, abs(H_fir_BP)),grid
title('Amplitudengang des Filters');
xlabel('f (Hz)')
ylabel('Die Amplitude')
% das verschobenen Filter
k=0:178;
b_Koeff_shifted= b_Koeff.*exp(2*1i*pi*k.*(225)/fA);
fvtool(b_Koeff_shifted,1) 
H_shifted =freqz(b_Koeff_shifted, 1, 2*pi*freq);
figure(11);
plot(freq*fA, abs(H_shifted)),grid
title('Amplitudengang des verschobenen Filters');
xlabel('f (Hz)')
ylabel('Die Amplitude')
[recieved_samples,Fs] = audioread('import2.wav');
recieved_samples = cpfsk_sig;
cpfsk_kom=filter(b_Koeff_shifted,1,recieved_samples);
cpfsk_kom_delayed = conj([0;cpfsk_kom(1:length(cpfsk_kom)-1)]);
y_FM_demod=cpfsk_kom.*cpfsk_kom_delayed;
y_cpfsk_out= atan2(imag(y_FM_demod),real(y_FM_demod));
figure(12);
plot(y_cpfsk_out);
ma = matched_filter(y_cpfsk_out,fs);
figure
plot(ma)
title('Das demodulierte CPFSK-Signal mit einem komplexen FIR-Filter (N=178)')
xlabel('Anzahl der Samples')
ylabel('Die Amplitude')