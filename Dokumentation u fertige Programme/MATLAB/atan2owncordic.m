function [theta,theta_test] = atan2owncordic(Y,X,iter)
%% ATAN2OWNCORDIC 
% Basierend auf der Dokumentation von STMircoelectronix
% => Arctangent => Vector =>  x=(1/An)x0, y=(1/An)y0, z=0 !=> Reduce y to Zero
% 
% 
% Beschreibung:
% Ziel ist die Phasenbestimmung durch Annäherung von Y an 0.
% Dazu wird bei jeder Iteration geguckt, ob y positiv oder negativ ist und
% entsprechend das Vorzeichen (als Variable z) der Rotationsrichtung (also
% +-°) gesetzt.
% je nach Literatur gibt es zwei Möglichkeiten, die endgültige Phase im 4
% Quadrantenbereich zu berechnen:
% Addition der vorweg abgezogenen Phase
% einmalige Multiplikation des Phasenergebnisses mit einem Vorzeichen (hier
% auch Vorzeichen genannt)
% auf eine Kompensation der Amplitude wird verzichtet, da nur die Phase
% relevant ist
%
% Ausgabe:
% theta in rad => Literatur
% theta_test in rad => offen für alternative Berechnung

% Abfangen, dass mehr Iterationen gemacht werden, als die LUT (siehe unten) hergibt!
if iter>100
    iter=100;
end


% erstmal die Phase auf 0
phase=0;
theta=0;
theta_test=0;
%variable (hochZwei) zur Darstellung 1/(2^i)  => In C Bitshift!
hZ=1; %Start 1/(2^0)=1

%laden der LUT (Look-Up-Table) enthält eine Tabelle in Form eines Arrays
%für atan( 1 / (2^i) ) mit i = 0 : MAX_ITERATIONEN
%MAX_ITERATIONEN ist aktuell 100. Je nach Literatur zwischen 15 und 20
%Durchläufen mindestens nötig für Genauigkeit.
load("owncordig_LUT.mat","owncordig_LUT");

%Welcher Quadrant?

% Wenn im 3. Quadranten (lu)
x=X;
y=Y;
vorzeichen=0;
if (x<0)
    if (y>0)
        vorzeichen=1;
    else
        vorzeichen=-1;
    x=-x;
    y=-y;
    end
end

% eigentlicher Algorithmus
for k=1:iter
   
    delta_phase=owncordig_LUT(k);
    if (y<=0)
        x1=x-(y*hZ);
        y1=y+(x*hZ);
        phase=phase-delta_phase;
    else
        x1=x+(y*hZ);
        y1=y-(x*hZ);
        phase=phase+delta_phase;
    end
    
    %Vorbereitung neue Iteration
    x=x1;
    y=y1;
    
    %Erhöhung des 1/2^k mit k+1 => halbiert / bitshift
    hZ=hZ/2;
end

%Berechnung der eigentlichen Phase
theta_test=theta_test; % Platz für alternative Methode
theta=phase; %Vorgehensweise der Literatur
end

