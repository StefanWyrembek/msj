 clear
 close all
 clc


fs = input('Bitte geben Sie die gewuenschte Abtastfrequenz ein: ');
input1 = input('Plot signal CPFSK ? Y/N [Y]: ','s');

[cpfsk_sig,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs,input1);                      % cpfsk Signal create durch function
clear input1;

% [cpfsk_sig,fs] = audioread('import_cpfsk.wav');
% fs = 5600;
% N = length(cpfsk_sig);
% n=(0:N-1)';
% maxval = max(abs(fft(cpfsk_sig))/N );

% input1 = input('How much noise in dB ? [100]: ');
% cpfsk_sig = awgn(cpfsk_sig, input1);

input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
[sig_filt_fir_real,sig_filt_fir_imag, fil_ord] = fir_filter(cpfsk_sig, input1,n, N, maxval, fs);         % apply fir, hilbert
clear input1;

test = hilbert(cpfsk_sig);
sig_fir = sig_filt_fir_real + 1i*sig_filt_fir_imag;                                               % gesamtsignal aus real und imag
sig_fir_delay=sig_fir.*conj([0;sig_fir(1:N-1)]);

%yq = sig_fir_delay.*exp(-1i*2*pi*fT*N-1);

ii  = sig_filt_fir_imag



theta2= atan2(imag(sig_fir_delay),real(sig_fir_delay));

I = sig_filt_fir_real;
Q = sig_filt_fir_imag;

input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
[I_o,uu1, fil_ord] = fir_filter(I, input1,n, N, maxval, fs);
Q_o =  I.*( [0;I(1:N-1)]);

[Q_u,uu2, fil_ord] = fir_filter(Q, input1,n, N, maxval, fs);
I_u =  Q.*( [0;Q(1:N-1)]);

Y_o = I_o + Q_o;
Y_u = I_u - Q_u;

theta2a = atan2(Y_o,Y_u );
Y_Matched =  matched_filter(theta2a);
% input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
% [II,IQ, fil_ord] = fir_filter(I, input1,n, N, maxval, fs);         % apply fir, hilbert
% clear input1;
% 
% 
% 
% 
% Ig = II + IQ;
% Ig_d = IQ.*( [0;IQ(1:N-1)]);
% 
% 
% input1 = input('Plot signal CPFSK filtered ? Y/N [Y]: ','s');
% [QI,QQ, fil_ord] = fir_filter(Q, input1,n, N, maxval, fs);         % apply fir, hilbert
% clear input1;
% 
% Qg = QI + QQ;
% Qg_d = Qg.*( [0;Qg(1:N-1)]);


% Y_I =  II-Qg_d;
% Y_Q = Ig_d+QI;
% 
% 
% Y = atan2(imag(Y_Q),real(Y_I));
% Y_M = matched_filter(Y);

%rec_sig_unwr1 = unwrap(Y_M);

[rec_sig] = matched_filter(theta2);

rec_sig_unwr = unwrap(rec_sig);

figure(10)
plot(theta2)

figure(20)
plot(rec_sig)

figure(30)
plot(rec_sig_unwr) %unwrap hat keinen Einfluss 


