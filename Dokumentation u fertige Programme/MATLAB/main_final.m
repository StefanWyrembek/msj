 clear all
 close all
 clc

% Rauschen in dB 
awgn_db = 100;

list = {'generate cpfsk_signal','use .wav'};
[indx] = listdlg('ListString',list);

switch indx
    
    case 1
        
        fs = input('Bitte geben Sie die gewuenschte Abtastfrequenz ein: ');
        
        % cpfsk Signal create durch function
        [cpfsk_sig,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs);
    
    case 2
        
        % cpfsk Signal create durch einlesen einer .wav Datei
        [cpfsk_sig,fs] = audioread('import2.wav');
        [~,cpfsk_sig_carrier, n, N, maxval, fT] = gen_cpfsk_signal(fs);
   
end

list2 = {'use only normal atan2()','compare with CORDIC'};
[indx2] = listdlg('ListString',list2);
switch indx2
    case 1
        cordic=0;    
    case 2
        cordic=1;
end


% add awgn Rauschen
cpfsk_sig = awgn(cpfsk_sig, awgn_db);

Tsym=0.04; %(Modulationsfrequenz 25Hz)


list3 = {'use hilbert','use cascade'};
[indx3] = listdlg('ListString',list3);
switch indx3
    case 1
        % Hilbert Transformator 
        %[x_i, x_q] = hilbert_trans(cpfsk_sig, n, N, maxval, fs);      
        [x_i, x_q] = hilbert_trans(cpfsk_sig);    
    case 2
        % Delay-cascade I/Q
        % Q-Signal ist das um 90° phasenverschobene I Signal
        % 90° = 1/4 einer Schwingung
        cascade=1;
        N_delays=round((Tsym/4)/(1/fs)); 
        x_i=cpfsk_sig;
        x_q=N_delay(cpfsk_sig,N_delays);
end





% Demodulation
list = {'real_baseband_delay_demod','complex_baseband_delay_demod','complex_filter','real_cmp_filter', 'compare plots'};

% Call different functions depending on user's selection
[indx] = listdlg('ListString',list);

switch indx
    
    case 1
        
        [y, t_c1, t_c2] = real_baseband_delay_demod(x_i, x_q, cordic);
        y_matched = matched_filter(y,fs);
        plot(y_matched);
        title('real baseband delay demod');
    case 2
       
        [y, t_c1, t_c2] = complex_baseband_delay_demod(x_i, x_q, cordic);
        y_matched = matched_filter(y,fs);
        plot(y_matched);
        title('complex baseband delay demod');
        
    case 3
        
        [y, t_c1, t_c2] = cmplx_demod(cpfsk_sig, fs, cordic);
        y_matched = matched_filter(y,fs);
        plot(y_matched)
        title('cmplx demod');
        
    case 4
        
        [y, t_c1, t_c2] = real_cmplx_demod(cpfsk_sig, fs, cordic);
        y_matched = matched_filter(y,fs);
        plot(y_matched)
        title('cmplx real demod');
    
    case 5 
        
        [y, ~, ~] = real_baseband_delay_demod(x_i, x_q,0);
        y_matched = matched_filter(y,fs);
        plot(y_matched);
        title('real baseband delay demod');
        
        figure
        [y1, ~, ~] = complex_baseband_delay_demod(x_i, x_q,0);
        y1_matched = matched_filter(y1,fs);
        plot(y1_matched);
        title('complex baseband delay demod');
        
        figure
        [y2, ~, ~] = cmplx_demod(cpfsk_sig, fs,0);
        y2_matched = matched_filter(y2,fs);
        plot(y2_matched)
        title('cmplx filter');
        
        figure
        [y3, t_c1, t_c2] = real_cmplx_demod(cpfsk_sig, fs,0);
        y3_matched = matched_filter(y3,fs);
        plot(y3_matched)
        title('cmplx real filter');
        
end

if (cordic==1)
    
    y_tc1=minus(y,t_c1);
    y_tc2=minus(y,t_c2);
    
    figure
    title('atan2() function compare')
    ylabel('Phase','FontSize',14)
    xlabel('samples (~t)','FontSize',14)
    hold on
    plot(y,'b')
    plot(t_c1,'r')
    plot(t_c2,'g')
    legend('original','MATLAB-CORDIC','own CORDIC')
    hold off
    
    figure
    plot((y_tc1),'b')
    title('differences between normal atan2() and both cordic')
    ylabel('Phasedifference','FontSize',14)
    xlabel('samples (~t)','FontSize',14)
    hold on
    plot((y_tc2),'g')
    legend('MATLAB to atan2()','OWN to atan(2)')
    hold off
    
end    

if cascade==1
    %%Phase
    %gibt die Phasendifferenz als einzelnen Wert an => kein Plot möglich!
    phasediff=phdiffmeasure(x_i,x_q); %Phase in Radians
    phasedegree = rad2deg(phasediff); %Phase in Grad

    %%Plot
    figure(70)
    plot(x_i,'b')
    hold on
    plot(x_q,'r')
    legend('I','Q')
    title('Compare I/Q')
    xlabel('samples','FontSize',14)
    ylabel('Amplitude','FontSize',14)
    hold off
end

list4 = {'no decode','sample compare','differentiator'};
[indx4] = listdlg('ListString',list4);
switch indx4
    case 1
            
    case 2
        
        [Decode_signal, Decode_symbol]= decode_easy01(y_matched,fs,Tsym);
        
        figure(80)
        plot(Decode_signal,'r')
        legend('Decoded Signal (1/0)')
        title('Decoded Signal')
        xlabel('samples','FontSize',14)
        ylabel('Value','FontSize',14)

        figure(90)
        plot(Decode_symbol,'r')
        legend('Decoded Symbols (1/0)')
        title('Decoded Symbols')
        xlabel('symbols','FontSize',14)
        ylabel('bit','FontSize',14)
        
    case 3
        
        [Decode_signal, Decode_symbol]= decode_diff(y_matched,fs,Tsym);
        
        figure(80)
        plot(Decode_signal,'r')
        legend('Decoded Signal (1/0)')
        title('Decoded Signal')
        xlabel('samples','FontSize',14)
        ylabel('Value','FontSize',14)

        figure(90)
        plot(Decode_symbol,'r')
        legend('Decoded Symbols (1/0)')
        title('Decoded Symbols')
        xlabel('symbols','FontSize',14)
        ylabel('bit','FontSize',14)
end

