% cpfsk_generator.m
%
% US 10-Apr-2019
% 
% MSJ SS 2019 : Bandbreite des CPFSK Signals bestimmen
clear
close all
clc

% Parameter
amplitude = 1;
fs = 5600;
%fs = 10000;
%fs = 9.978560e3;
%fT = 10100800;
fT = fs/4;
half_of_freq_shift = 225 ;
phi_0 = 0;

% Ein Bit hat eine L�nge von 20 msec, also 0.02 sec. Die 30 msec vom
% Stopbit ignorieren wir erstmal...
disp('--------------------------------------------')
samples_pro_bit = 0.02 * fs;
fprintf('samples_pro_bit : %d\n', samples_pro_bit);

rect_1_to_modulate_basic = 2 * ([ones(samples_pro_bit,1);zeros(samples_pro_bit,1)]-0.5);
num_rect_1_to_modulate_basic = length(rect_1_to_modulate_basic);

fprintf('L�nge einer 0/1 Bit Folge : %d\n', length(rect_1_to_modulate_basic) );
fm = 1/length(rect_1_to_modulate_basic)*fs;
fprintf('Modulationsfrequenz, wenn es immer mit 0/1/0/1 usw weitergeht : %d Hz\n', fm );
disp('--------------------------------------------')
rect_1_to_modulate = [];

% 10 Pakete hintereinander ...
num_intervals = 10;
for kx=1:num_intervals
    rect_1_to_modulate = [rect_1_to_modulate; rect_1_to_modulate_basic];
end;
N = length(rect_1_to_modulate);
n=(0:N-1)';

num=[0,1];
den=[1,-1];
triag_to_modulate = filter(num, den, rect_1_to_modulate);

cpfsk_sig         = amplitude .*  sin(2 * pi * fT * n/fs + 2 * pi * half_of_freq_shift * triag_to_modulate/fs + phi_0);
cpfsk_sig_carrier = amplitude .*  sin(2 * pi * fT * n/fs + phi_0);
maxval = max(abs(fft(cpfsk_sig))/N );

figure(1);
set(gcf,'Units','normal','Position',[0.1 .4 .4 .4])
plot(n, cpfsk_sig_carrier,'b.-', n, rect_1_to_modulate,'r-'),grid
axis([0,N,-1.2,1.2]);

figure(2);
set(gcf,'Units','normal','Position',[0.2 .4 .4 .4])
plot(n/N*fs, abs(fft(cpfsk_sig))/N/maxval),grid

figure(3);
set(gcf,'Units','normal','Position',[0.3 .4 .4 .4])
freq = n/N*fs;
subplot(2,1,1);
stem(freq, abs(fft(cpfsk_sig))/N/maxval,'filled'),grid
axis([900,1800,-0.1, +1.2]);
subplot(2,1,2);
stem(freq, abs(fft(cpfsk_sig))/N/maxval,'filled'),grid
axis([900,1800,-0.1, +0.2]);

%%
%Hilbert Filter Methode
%hilbert_f = hilbert(cpfsk_sig); %%ab hier ist Signal sig_img, sig_real

%sig_hilbert_real = real(hilbert_f);
%sig_hilbert_imag = imag(hilbert_f);  

%figure(4);
%set(gcf,'Units','normal','Position',[0.1 .4 .4 .4])
%plot(n, sig_hilbert_real,'b.-', n, sig_hilbert_imag,'r-'),grid
%axis([0,N,-1.2,1.2]);

%%FIR-Methode
b=firpm(22,[0.1 0.9],[1 1], 'hilbert');                                 %berechnuhng der Koeffizienten des Filters
fvtool(b,1)                                                             %Amplitudengang des Filters
sig_filt_fir_imag = filter(b,1,cpfsk_sig);                              %das ist der Imagin�rpart des Analytischen Signals
sig_filt_fir_real = [zeros(11,1); cpfsk_sig(1:length(cpfsk_sig)-11)];   %das ist der Realpart des Analytischen Signals

sig_fir = sig_filt_fir_real+j*(sig_filt_fir_imag);

%xx = fmdemod(sig_fir,fT,fs);                                           %%hier sehen wir einen Fehler Y must be real --> arctan

figure(5);
set(gcf,'Units','normal','Position',[0.2 .4 .4 .4])
plot(n/N*fs, abs(fft(sig_fir))/N/maxval),grid

figure(6);
set(gcf,'Units','normal','Position',[0.1 .4 .4 .4])
plot(n, sig_fir,'b.-'),grid
axis([0,N,-1.2,1.2]);

theta = atan(sig_filt_fir_imag./sig_filt_fir_real);                     %determining the phase angle, tan^-1 = atan
deg = rad2deg(theta);
theta_2 = atan2(sig_filt_fir_imag,sig_filt_fir_real);    
sig_fir_phase = sig_filt_fir_real.*exp(j*theta);                         %das ist z = x+jy, wie sig_fir 
sig_demod = fmdemod(cpfsk_sig,fT,fs,225);
sig_demod_fir = fmdemod(real(sig_fir),fT,fs,225);                          %demod um 10 samples versetzt

